<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->unsignedBigInteger('tag_id')->after('id');
            $table->unsignedBigInteger('user_id')->after('id');
            // $table->decimal('price', 8 ,2)->nullable(true)->change();
            // $table->integer('price')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropColumn('tag_id');
            $table->dropColumn('user_id');
            // $table->decimal('price', 8 ,2)->nullable(false)->change();
            // $table->decimal('price', 8 ,2)->change();
        });
    }
}
