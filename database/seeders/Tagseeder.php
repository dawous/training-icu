<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class Tagseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->truncate();
        DB::table('tags')->insert([
            'tag_name' => 'Gadget',
            'description' => 'All gadget'
        ]);

        DB::table('tags')->insert([
            'tag_name' => 'Technology',
            'description' => 'All technology'
        ]);
    }
}
