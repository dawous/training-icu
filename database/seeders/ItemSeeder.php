<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->truncate();
        DB::table('items')->insert([
            'user_id' => 1,
            'tag_id' => 1,
            'item_name' => 'Laptop',
            'price' => '2500',
        ]);

        DB::table('items')->insert([
            'user_id' => 1,
            'tag_id' => 2,
            'item_name' => 'Handphone',
            'price' => '4400',
        ]);
    }
}
