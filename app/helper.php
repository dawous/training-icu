<?php

use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;

function encrypt_string($value)
    {
        return Crypt::encryptString($value);
    }

function decrypt_string($value)
    {
        try {
            $decrypted = Crypt::decryptString($value);
            return $decrypted;
        } catch (DecryptException $e) {
            abort(404);
        }
    }
