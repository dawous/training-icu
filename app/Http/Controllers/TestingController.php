<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemRequest;
use App\Models\Item;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TestingController extends Controller
{
    public function form()
    {
        $tags = Tag::get();
        return view('validation-form',compact('tags'));
    }

    public function store(ItemRequest $request)
    {
        $item = new Item();
        $item->user_id = \Auth::user()->id;
        $item->tag_id = $request->tag;
        $item->item_name = $request->item_name;
        $item->price = $request->price;
        $item->save();

        // Session::flash('status', 'berjaya');

        return redirect()->route('modul.validation-form')->with('success','Produk berjaya didaftarkan');
        //store process
        // $rule = [
        //     // 'item_name' => 'required | string'
        //     'item_name' => ['required','string','unique:items,item_name'],
        //     'price' => ['required','regex:/^\d*(\.\d{2})?$/']
        // ];

        // $message = [
        //     // 'required' => 'Ruang :attribute perlu diisi',
        //     // 'price.required' => 'Borang :attribute kene isi',
        //     // 'price.regex' => 'Ruang :attribute tidak sah'
        // ];

        // $attribute = [
        //     'item_name' => 'nama produk',
        //     'price' => 'harga'
        // ];

        // $this->validate($request,$rule,$message,$attribute);
    }
}
