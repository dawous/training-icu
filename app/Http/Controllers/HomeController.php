<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function papar()
    {
        //process
        echo "Hello World";
    }

    public function register()
    {
        $name = Auth::user()->name;
        $data = "<p>Saya bernama abu</p><h1>Umo 2 tahun</h1>";

        return view('registration',compact('name','data'));

        // return view('registration',['name'=> $name]);
    }
}
