<?php

namespace App\Http\Controllers;

use App\Http\Requests\ItemRequest;
use App\Http\Requests\ItemRequestUpdate;
use App\Models\Item;
use App\Models\Tag;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class ItemController extends Controller
{
    // public function index()
    // {

    //     if(!request()->filled('size')){
    //         $size = 2;
    //     }else{
    //         $size = request()->size;
    //     }

    //     //select * from users where ((name  = "firdaus" or age = 30) or race= malay)

    //     $search = request()->search;

        //     $data = Item::with(['user','tag'])
        //             ->when(request()->filled('search'),function($q) use ($search){

        //                 $q->where(function($q) use ($search){ //grouping function
        //                     $q->where('item_name','like','%'.$search.'%');

        //                     $q->orWhere('description','like','%'.$search.'%');

        //                     $q->orWhereHas('user',function($query) use ($search){
        //                         $query->where('name',$search);
        //                     });

        //                     $q->orWhereHas('tag',function($query) use ($search){
        //                         $query->where('tag_name',$search);
        //                     });
        //                 });

    //             })->paginate($size);

    //     return view('item.index',compact('data'));
    // }

    public function index()
    {

        $data = Item::with(['user','tag'])->orderBy('created_at','DESC')->paginate(5);

        return view('item.index-2',compact('data'));
    }

    function fetch_data(Request $request)
    {
     if($request->ajax())
     {
        $sort_by = $request->get('sortby');
        $sort_type = $request->get('sorttype');
        $search = $request->get('query');
        $search = str_replace(" ", "%", $search);
        $entry = (request()->filled('entry')) ? request()->get('entry') : 2 ;

        $data = Item::with(['user','tag'])
        ->when(request()->filled('query'),function($q) use ($search){

            $q->where(function($q) use ($search){ //grouping function
                $q->where('item_name','like','%'.$search.'%');

                $q->orWhere('description','like','%'.$search.'%');

                $q->orWhereHas('user',function($query) use ($search){
                    $query->where('name',$search);
                });

                $q->orWhereHas('tag',function($query) use ($search){
                    $query->where('tag_name',$search);
                });
            });

        })
        ->orderBy($sort_by, $sort_type)
        ->paginate($entry);
        return view('item.pagination-data', compact('data'))->render();
        }
    }

    public function create()
    {
        $tags = Tag::get();
        $item = new Item();
        return view('item.create',compact('tags','item'));
    }

    public function store(ItemRequest $request)
    {
        $item = new Item();
        $item->user_id = Auth::user()->id;
        $item->tag_id = $request->tag;
        $item->item_name = $request->item_name;
        $item->price = $request->price;
        $item->description = $request->description;
        $item->save();

        // Session::flash('status', 'berjaya');

        return redirect()->route('modul.item.index')->with('success','Item successfully registered');
    }

    public function edit($id)
    {
        $id = decrypt_string($id);

        $item = Item::findOrFail($id);
        $tags = Tag::get();
        return view('item.edit',compact('item','tags'));
    }

    public function update(ItemRequestUpdate $request,$id)
    {
        $id = decrypt_string($id);

        $item = Item::findOrFail($id);
        $item->tag_id = $request->tag;
        $item->item_name = $request->item_name;
        $item->price = $request->price;
        $item->description = $request->description;
        $item->save();

        return redirect()->route('modul.item.index')->with('success','Item successfully updated');
    }

    public function destroy($id)
    {
        $id = decrypt_string($id);

        $item = Item::findOrFail($id);
        $item->delete();

        return redirect()->route('modul.item.index')->with('success','Item successfully deleted');
    }
}
