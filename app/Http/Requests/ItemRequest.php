<?php

namespace App\Http\Requests;

use App\Models\Item;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd(request()->all());


        return [
            // 'item_name' => ['required','string',Rule::unique('items')->where(function($q){
            //     $q->where('tag_id',request()->tag);
            // })],
            'item_name' => ['required','string', function ($attribute, $value, $fail) {
                $item = Item::where('item_name','=',$value)->where('tag_id',request()->tag)->exists();

                if($item){
                    $fail('Maklumat untuk ruang nama produk telah digunakan');
                }
            }],
            'price' => ['required','regex:/^\d*(\.\d{2})?$/']
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Ruang :attribute perlu diisi',
            'price.required' => 'Borang :attribute kene isi',
            'price.regex' => 'Ruang :attribute tidak sah'
        ];
    }

    public function attributes()
    {
        return [
            'item_name' => 'nama produk',
            'price' => 'harga'
        ];
    }
}
