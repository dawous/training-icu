<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    protected $table = 'items';

    //  protected $primaryKey = 'uuid';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class,'tag_id','id');
    }

}
