<div class="form-group">
    <label for="exampleInputEmail1">Item Name:</label>
    <input type="text" name="item_name" autocomplete="off" value="{{ old('item_name',$item->item_name)}}" class="form-control @error('item_name') is-invalid" @enderror  placeholder="Enter item name"  aria-invalid="true">
    @error('item_name')
        <span class="error invalid-feedback">{{$message}}</span>
    @enderror
</div>
<div class="form-group">
    <label>Item Price:</label>
    <input type="text" name="price" value="{{ old('price',$item->price)}}" autocomplete="off"  class="form-control @error('price') is-invalid" @enderror  placeholder="Enter item price"  aria-invalid="true">
    @error('price')
        <span class="error invalid-feedback">{{$message}}</span>
    @enderror
</div>

<div class="form-group">
    <label>Item Description:</label>
    <textarea type="text" name="description"   class="form-control @error('description') is-invalid @enderror ">{{old('description',$item->description)}}</textarea>
    @error('description')
        <span class="error invalid-feedback">{{$message}}</span>
    @enderror
</div>

<div class="form-group">
    <label>Tag:</label>
    <select name="tag" class="form-control">
        <option >Select</option>
        @foreach ($tags as $tag)
            <option value="{{ $tag->id }}" @if(old('tag',$item->tag_id) == $tag->id) selected @endif>{{$tag->tag_name}}</option>
        @endforeach
    </select>
    @error('tag')
        <span class="error invalid-feedback">{{$message}}</span>
    @enderror
</div>

<button type="submit" class="btn btn-primary">{{$button}}</button>
