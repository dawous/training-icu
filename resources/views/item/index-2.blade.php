@extends('layouts.template-1.app')

@section('title','List of item')

@section('content')

<a href="{{ route('modul.item.create')}}" class="btn btn-primary float-right">Add Item</a>
<div class="row mb-2">
    <div class="col-md-2">
        <select class="form-control" id="entry">
            <option value ="5">5</option>
            <option value ="10">10</option>
            <option value ="15">15</option>
        </select>
    </div>
    <div class="col-md-10">
        <input type="text" placeholder="Search" id="search" class="form-control">
    </div>
</div>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th class="sorting" data-sorting_type="asc" data-column_name="item_name" style="cursor: pointer">Item Name<span id="item_name_icon"></span></th>
                <th>Tag</th>
                <th class="sorting" data-sorting_type="asc" data-column_name="price" style="cursor: pointer">Price <span id="price_icon"></span></th>
                <th>Description</th>
                <th>Owner</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="item-data">
           @include('item.pagination-data')
        </tbody>
    </table>
    <input type="hidden" name="hidden_page" id="hidden_page" value="1" />
    <input type="hidden" name="hidden_column_name" id="hidden_column_name" value="item_name" />
    <input type="hidden" name="hidden_sort_type" id="hidden_sort_type" value="asc" />

</div>
@endsection
@push('js')
    <script>
        $(document).ready(function(){



            $( ".confirmation" ).click(function() {
                var id = $(this).data('id');
                var title = $(this).data('title');
                Swal.fire({
                    title: 'Confirmation',
                    html: "Are you sure you want to remove <b>"+title.toUpperCase()+"</b>? You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#6259ca',
                    cancelButtonColor: '#f16d75',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        $(id).submit();
                    }
                });
            })


            function clear_icon()
            {
                $('#item_name_icon').html('');
                $('#price_icon').html('');
            }


            function fetch_data(page, sort_type, sort_by, query, entry)
            {
                $.ajax({
                    url:"/modul/item/fetch_data?page="+page+"&sortby="+sort_by+"&sorttype="+sort_type+"&query="+query+"&entry="+entry,
                    success:function(data)
                    {
                        $('#item-data').html('');
                        $('#item-data').html(data);
                    }
                })
            }

            $(document).on('keyup', '#search', function(){
                var query = $('#search').val();
                var column_name = $('#hidden_column_name').val();
                var sort_type = $('#hidden_sort_type').val();
                var page = $('#hidden_page').val();
                var entry = $('#entry').val();
                fetch_data(page, sort_type, column_name, query, entry);
            });

            $(document).on('change', '#entry', function () {
                var query = $('#search').val();
                var column_name = $('#hidden_column_name').val();
                var sort_type = $('#hidden_sort_type').val();
                var page = 1;
                var entry = $('#entry').val();
                fetch_data(page, sort_type, column_name, query, entry);
            });

            $(document).on('click', '.sorting', function(){
                var column_name = $(this).data('column_name');
                var order_type = $(this).data('sorting_type');
                var reverse_order = '';
                if(order_type == 'asc')
                {
                    $(this).data('sorting_type', 'desc');
                    reverse_order = 'desc';
                    clear_icon();
                    $('#'+column_name+'_icon').html('<i class="fa fa-sort-asc"></i>');
                }

                if(order_type == 'desc')
                {
                    $(this).data('sorting_type', 'asc');
                    reverse_order = 'asc';
                    clear_icon
                    $('#'+column_name+'_icon').html('<i class="fa fa-sort-desc"></i>');
                }

                $('#hidden_column_name').val(column_name);
                $('#hidden_sort_type').val(reverse_order);

                var page = $('#hidden_page').val();
                var query = $('#search').val();
                var entry = $('#entry').val();

                fetch_data(page, reverse_order, column_name, query, entry);

            });

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault();

                var page = $(this).attr('href').split('page=')[1];
                $('#hidden_page').val(page);

                var column_name = $('#hidden_column_name').val();
                var sort_type = $('#hidden_sort_type').val();
                var query = $('#search').val();
                var entry = $('#entry').val();

                $('li').removeClass('active');
                $(this).parent().addClass('active');

                fetch_data(page, sort_type, column_name, query, entry);
            });


        });
    </script>
@endpush
