@extends('layouts.template-1.app')

@section('title','Edit Item')

@section('content')
    <form action="{{route('modul.item.update',encrypt_string($item->id))}}" method="post">
        @csrf
        @include('item.form',['button'=>'update'])
    </form>
@endsection


