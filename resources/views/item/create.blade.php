@extends('layouts.template-1.app')

@section('title','Add New Item')

@section('content')
    <form action="{{route('modul.item.store')}}" method="post">
        @csrf
        @include('item.form',['button'=>'save'])
    </form>
@endsection


