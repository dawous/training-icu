@extends('layouts.template-1.app')

@section('title','List of item')

@section('content')
<div class="row mb-2">
    <div class="col-md-2">
        <select class="form-control" id="size" onchange="table_size(this.value)">
            <option @if(request()->size == 2) selected @endif value ="2">2</option>
            <option @if(request()->size == 10) selected @endif value ="10">10</option>
            <option @if(request()->size == 15) selected @endif value ="15">15</option>
        </select>
    </div>
    <div class="col-md-10">
        <input type="text" placeholder="Search" id="search" value="{{request()->search}}" class="form-control" onkeydown="search(this.value)">
    </div>
</div>
<div class="table-responsive">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Item Name</th>
                <th>Tag</th>
                <th>Price</th>
                <th>Description</th>
                <th>Owner</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse($data as $key => $item)
                <tr>
                    <td>{{$data->firstItem() + $key}}</td>
                    <td>{{$item->item_name}}</td>
                    <td>{{$item->tag->tag_name}}</td>
                    <td>{{number_format($item->price,2)}}</td>
                    <td>{!! $item->description !!}</td>
                    <td>{{$item->user->name}}</td>
                    <td>
                        <a href="" class="btn btn-info"><i class="fas fa-edit"></i></a>
                        <a href="" class="btn btn-danger"><i class="fas fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">No record</td>
                </tr>
            @endforelse
        </tbody>
    </table>

    {{ $data->appends(['size'=> request()->size, 'search' => request()->search])->links('vendor.pagination.bootstrap-4')}}
</div>
@endsection
@push('js')
    <script>
        function table_size(size)
        {
            var search = $("#search").val();
            window.location.replace("/modul/item/index?search="+ search +"&size=" + size);
        }

        function search(search)
        {
            var size = $("#size").val();
            if(event.key === 'Enter') {
                window.location.replace("/modul/item/index?search=" + search + "&size=" + size);
            }
        }
    </script>
@endpush
