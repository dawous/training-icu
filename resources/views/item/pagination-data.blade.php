@forelse($data as $key => $item)
<tr>
    <td>{{$data->firstItem() + $key}}</td>
    <td>{{$item->item_name}}</td>
    <td>{{$item->tag->tag_name}}</td>
    <td>{{number_format($item->price,2)}}</td>
    <td>{!! $item->description !!}</td>
    <td>{{$item->user->name}}</td>
    <td>
        <a href="{{ route('modul.item.edit', encrypt_string($item->id))}}" class="btn btn-info"><i class="fas fa-edit"></i></a>
        {{-- <a href="javascript:void(0)" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-data-{{$item->id}}').submit();"><i class="fas fa-trash"></i></a> --}}
        <a href="javascript:void(0)" class="btn btn-danger confirmation" data-id="#delete-data-{{$item->id}}" data-title="{{$item->item_name}}"><i class="fas fa-trash"></i></a>
        <form id="delete-data-{{$item->id}}" action="{{ route('modul.item.destroy',encrypt_string($item->id)) }}" method="POST" class="d-none">
            @csrf
        </form>
    </td>
</tr>

@empty
    <tr>
        <td colspan="7">No record</td>
    </tr>
@endforelse
<tr class="border-bottom">
    <td colspan="7">
        {!!  $data->links('vendor.pagination.bootstrap-4') !!}
    </td>
</tr>
