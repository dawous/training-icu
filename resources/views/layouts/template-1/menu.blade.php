<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
      <li class="nav-item menu-open">
        <a href="#" class="nav-link">
          <i class="nav-icon fas fa-tachometer-alt"></i>
          <p>
            Modul Item
            <i class="right fas fa-angle-left"></i>
          </p>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="{{route('modul.item.index')}}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Item List</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Inactive Page</p>
            </a>
          </li>
        </ul>
      </li>
      {{-- <li class="nav-item">
        <a href="{{ route('home')}}" class="nav-link">
          <i class="nav-icon fas fa-th"></i>
          <p>
            Testing Route
          </p>
        </a>
      </li> --}}
      <li class="nav-item">
        <a href="{{ url('/home')}}" class="nav-link">
          <i class="nav-icon fas fa-th"></i>
          <p>
            Testing Route
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ route('modul.papar_static_page')}}" class="nav-link">
          <i class="nav-icon fas fa-desktop"></i>
          <p>
            Papar Static Page
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ route('modul.registration')}}" class="nav-link">
          <i class="nav-icon fas fa-user"></i>
          <p>
            Testing Middleware
          </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ route('modul.validation-form')}}" class="nav-link">
          <i class="nav-icon fas fa-user"></i>
          <p>
           Validation Form
          </p>
        </a>
      </li>


    </ul>
  </nav>
