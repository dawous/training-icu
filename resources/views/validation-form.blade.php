@extends('layouts.template-1.app')

@section('title','Validation Form')

@section('content')
    <form action="{{route('modul.validation-store')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleInputEmail1">Item Name:</label>
            <input type="text" name="item_name" value="{{ old('item_name')}}" class="form-control @error('item_name') is-invalid" @enderror  placeholder="Enter item name"  aria-invalid="true">
            @error('item_name')
                <span class="error invalid-feedback">{{$message}}</span>
            @enderror
        </div>
        <div class="form-group">
            <label>Item Price:</label>
            <input type="text" name="price" value="{{ old('price')}}"  class="form-control @error('price') is-invalid" @enderror  placeholder="Enter item price"  aria-invalid="true">
            @error('price')
                <span class="error invalid-feedback">{{$message}}</span>
            @enderror
        </div>

        <div class="form-group">
            <label>Tag:</label>
            <select name="tag" class="form-control">
                <option >Select</option>
                @foreach ($tags as $tag)
                    <option value="{{ $tag->id }}" @if(old('tag') == $tag->id) selected @endif>{{$tag->tag_name}}</option>
                @endforeach
            </select>
            @error('tag')
                <span class="error invalid-feedback">{{$message}}</span>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>
@endsection


