<?php

use App\Http\Controllers\FormController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\TestingContoller;
use App\Http\Controllers\TestingController;
use App\Models\Item;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



//retrieve page or data
Route::get('/home', [HomeController::class, 'index'])->middleware('auth')->name('home');

// Route::get('testing_function',function(){
//     //process
//     echo "Hello World";
// });


Route::name('modul.')->prefix('modul')->middleware(['auth'])->group(function(){
    Route::get('function_success',[HomeController::class, 'papar'])->name('function_success');

    //papar static age
    Route::view('papar_static_page','welcome')->name('papar_static_page');

    Route::get('registration',[HomeController::class, 'register'])->middleware('CheckingAge')->name('registration');

    Route::get('validation-form',[TestingController::class, 'form'])->name('validation-form');

    Route::post('validation-store',[TestingController::class, 'store'])->name('validation-store');

    Route::name('item.')->prefix('item')->group(function(){
        Route::get('index',[ItemController::class,'index'])->name('index');
        Route::get('create',[ItemController::class,'create'])->name('create');
        Route::post('store',[ItemController::class,'store'])->name('store');
        Route::get('fetch_data',[ItemController::class,'fetch_data'])->name('fetch_data');
        Route::get('{id}/edit',[ItemController::class,'edit'])->name('edit');
        Route::post('{id}/update',[ItemController::class,'update'])->name('update');
        Route::post('{id}/destroy}',[ItemController::class,'destroy'])->name('destroy');
    });
});

Route::get('testing-query',function(){
    /*------Retrieve all data----*/
    $query1 = Item::all();

    $query2 = Item::findOrFail(1);

    /*------Retrieve one data where item_name like lap----*/
    $query3 = Item::where('item_name','like','%lap%')->first();

    /*------Retrieve all data----*/
    $query4 = Item::get();

    /*------Checking existen of senduk in item table----*/
    $query5 = Item::where('item_name','senduk')->exists();

    /*------get sum price of items----*/
    $query6 = Item::where('user_id','=',1)->sum('price');

    /*------Retrieve one data where item_name like lap and user_id = 1 ----*/
    $query7 = Item::where('item_name','like','%lap%')->where('user_id','=',1)->first();

    /*------Retrieve one data where item_name like lap or user_id = 1 ----*/
    $query8 = Item::where('item_name','like','%lap%')->orWhere('user_id','=',1)->first();

    /*------Retrieve number of row found where user_id = 1 ----*/
    $query9 = Item::where('user_id','=',1)->count();

    /*------get average price of items where user_id = 1----*/
    $query10 = Item::where('user_id','=',1)->avg('price');

    /*------get data where condition from other table in relationship----*/
    $request_name = 'daus';

    $query11 = Item::whereHas('user',function($q) use ($request_name){
        $q->where('name','like','%'. $request_name.'%');
    })->get();

    /*------get user id=2 who has items----*/
    $query12 = User::whereHas('item')->get();

     /*------get data where condition from other table in relationship if $request not empty----*/
    $request = 'Handphone';

    $query13 = Item::when(($request != null or $request != ''),function($query)use ($request){
        $query->where(function($query) use ($request){
            $query->whereHas('user',function($q) use ($request){
                $q->where('name','like','%'. $request.'%');
            });
            $query->orWhere('item_name','like','%'. $request.'%');
        });
    })
    ->get();

    /*------how to use raw query----------*/
    $value = 'aus';
    $query14 = User::whereRaw(' lower(name) like :value', ['value' => '%'.strtolower($value).'%'])->first();

    $query14 = User::whereRaw(' lower(name) like ?', ['%'.strtolower($value).'%'])->first();

    // dd($query1,$query2,$query3,$query5,$query6,$query7, $query8, $query9, $query10);

    /*------Query builder join table-----------*/
    $query15 = DB::table('users as u')
                ->select('u.id as user_id','t.id as tag_id','i.id as item_id','u.name','tag_name','item_name')
                ->join('items as i','i.user_id','=','u.id')
                ->join('tags as t','i.tag_id','=','t.id')
                ->where('t.id','=',1)
                ->get();

    dd($query15);
});


